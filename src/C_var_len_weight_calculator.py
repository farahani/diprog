
import random
import math


class C_weight_calculator:
    def __init__(self,tree_width,vertex_number,data,epsilon):
        self.tree_width=tree_width
        self.data=data
        self.vertex_number=vertex_number
        self.epsilon=epsilon
        self.selector_graph_stat={}
        self.selector_graph_probs={}
        #making the root_set
        self.root_set=set([])
        for r in range(self.tree_width):
            self.root_set.add(str(r))
        
    def comb_list(self,seq, k):
        "returns a list of all k-combinations of the elements of sequence seq"
        n=len(seq)
        if not 0<=k<=n:
            raise Exception,"0<=k<=len(seq) is not true"
        v=[]   #list of combinations

        def f(x,y,a):
            if x==k:
                #we have taken enough elements, reject all remaining elements
                v.append(a)
                return
            if y==n-k:
                #we have rejected enough elements, take all remaining elements
                a.extend(seq[x+y:])
                v.append(a)
                return
            if (x<k):
                #take element seq[x+y]
                h=a+[seq[x+y]]
                f(x+1,y,h)
            if (y<n-k):
                #don't take element seq[x+y]
                f(x,y+1,a)            
        f(0,0,[])
        #print len(v)
        return v
    
    def synthetic_data_file_reader(self,file_path):
        data=[]
        infile=open(file_path)
        for line in infile:
            tmp=line.strip().split(',')
            data.append(tmp)
        return data
    
    def list_string_converter(self,par_list):

        res_list=[]
        for i in range(len(par_list)):
            tmp=''
            for j in range(len(par_list[i])):
                tmp+=','+str(par_list[i][j])
            res_list.append(tmp[1:])
        return res_list
        
    def generate_node_comb_dict(self,bag_size):
     
        res_dict={}
        combs_number=int(math.pow(2, bag_size))
        #print combs_number
        for i in range(combs_number):
            #checking to adding zeros to the left side.
            row_bin=str(bin(i)[2:])
            if len(row_bin) !=bag_size:
                for j in range(bag_size-len(row_bin)):
                    row_bin='0'+row_bin
                    
            res_dict[row_bin]=0
        return res_dict
        
    def hyper_edge_generator(self):

        node_number=self.vertex_number
        tree_width=self.tree_width
        
        par_dict={}
       
        temp_dict={}

    
        for tree_w in range(1,tree_width+1):

            par_dict[tree_w]=self.list_string_converter(self.comb_list(range(node_number+tree_width), tree_w))
         

        for tw in range(1,tree_width+1):
           
            tmp_dict={}

            for child_node in range(tree_width,node_number+tree_width):
                for par_comb in par_dict[tw]:
                    if str(child_node) not in par_comb.split(','): #and (str(child_node) not in self.root_set):
                        tmp_dict[par_comb+','+str(child_node)]=self.generate_node_comb_dict(tw+1) # write a funcion to produce {'111':0,'101':1,...}
            temp_dict[tw]=tmp_dict


        return temp_dict
    
    def zero_one_string_maker(self,hyper_edge_vertex_list,tumor):
        # gets a SINGLE tumor and the vertices in a hyper edge and returns the corresponding 1100111 list
        zero_one_string=''
        for v in hyper_edge_vertex_list:
            if v in tumor:
                zero_one_string+='1'
            else:
                zero_one_string+='0'
        return zero_one_string
    
    def stat_dict_maker(self):

        stat_dict=self.hyper_edge_generator()
        for tw in range(1,self.tree_width+1):
            for k,v in stat_dict[tw].iteritems():
                #list of vertices in the current hyperedge, the last one is the child vertex
                hyper_edge_vertex_list=k.split(',')
                for i in range(len(self.data)):
                    zero_one_string=self.zero_one_string_maker(hyper_edge_vertex_list,self.data[i])
                    #adding the instance to the selector_graph
                    stat_dict[tw][k][zero_one_string]+=1
        self.selector_graph_stat=stat_dict           
        return stat_dict
    

                    
    def parents_stat_finder(self,parents_list,parent_comb):
        # finds the population of the parents in the self.selector_graph_stat
        parent_zo_dict={}
        #this variable contains the combination of one and zeros in the selector graph
        new_zo_combination=''
        parents_stat=0

        #print parents_list
        #print parent_comb
        parents_set=set(parents_list)
        # assigning the right values to parent variables
        for i in range(len(parents_list)):
            parent_zo_dict[parents_list[i]]=parent_comb[i]
        

        
        for k in self.selector_graph_stat[len(parents_list)-1].keys():

            if parents_set==set(k.split(',')):
                #print parents_set
                new_parent_order=k.split(',')
                for v in new_parent_order:
                    new_zo_combination+=parent_zo_dict[v]
                
                parents_stat=self.selector_graph_stat[len(parents_list)-1][k][new_zo_combination]
                break

        #print parent_comb,parents_list
        return parents_stat
           
           
           
    def vertex_stat_dict_maker(self):   

        vertex_stat_dict={}
    
        for v in range(self.vertex_number+self.tree_width):
            vertex_stat_dict[str(v)]={'1':0,'0':0}
        for v in range(self.vertex_number+self.tree_width):
            for i in range(len(self.data)):
                if str(v) in self.data[i]:
                    vertex_stat_dict[str(v)]['1']+=1
                else:
                    vertex_stat_dict[str(v)]['0']+=1        
                    
        return vertex_stat_dict     
    
    def probability_calculator(self):
        # the prob_dict dictionary looks like this prob_dict[tree_width]['3,5,7']={'111':0,'101':0,...}
        prob_dict={}
        prob_dict=self.hyper_edge_generator()
        
       
        vertex_stat_dict={}
        #print 'vertex is'+str(self.vertex_number)
        for v in range(self.vertex_number+self.tree_width):
            vertex_stat_dict[str(v)]={'1':0,'0':0}
        for v in range(self.vertex_number+self.tree_width):
            for i in range(len(self.data)):
                if str(v) in self.data[i]:
                    vertex_stat_dict[str(v)]['1']+=1
                else:
                    vertex_stat_dict[str(v)]['0']+=1

        # calculating probabilities for tree-width 1
        for k,v in self.selector_graph_stat[1].iteritems():
            parent_vertex=k.split(',')[0]
            for comb,stat in v.iteritems():
                parent_vertex_status=comb[0]
                ####### Achtung: division by zero danger, the singular status should be investigated 
                if vertex_stat_dict[parent_vertex][parent_vertex_status]!=0:
                    prob_dict[1][k][comb]=float(self.selector_graph_stat[1][k][comb])/vertex_stat_dict[parent_vertex][parent_vertex_status]
                               
        # calculating probabilities for tree-width > 1
        for tw in range(2,self.tree_width+1):
            for k,v in self.selector_graph_stat[tw].iteritems():
                edg=k.split(',')
                for comb,stat in v.iteritems():


                    parents_stat=self.parents_stat_finder(edg[0:len(edg)-1], comb[0:len(comb)-1])
                    #else:
                        #parents_stat=0
                      
                    if parents_stat!=0:
                        # if parent_stat==0 then later we replace it with an epsilon
                        prob=float(stat)/parents_stat
                        prob_dict[tw][k][comb]=prob 
                        
        self.selector_graph_probs=prob_dict
        return prob_dict
        
    def weight_calculator(self):
        
        stat_dict=self.stat_dict_maker()
        probs_dict=self.probability_calculator()
        weight_dict={}
        
        samples_no=len(self.data)
        ep=float(1)/math.pow(samples_no,2)
        
        for tw in range(1,self.tree_width+1):
            weight_dict[tw]={}
            for k,v in stat_dict[tw].iteritems():
                tmp=0
                for comb in v.keys():
                    if probs_dict[tw][k][comb] >0:
                        tmp+=stat_dict[tw][k][comb]*math.log(probs_dict[tw][k][comb])
                    else:
                      
                        tmp+=math.log(self.epsilon*1e-10)

                        
                weight_dict[tw][k]=tmp-0.5*math.log(len(self.data))*math.pow(2, int(tw))
                
        return weight_dict
    
    
    
    def monotone_weight_calculator(self):
        stat_dict=self.stat_dict_maker()
        probs_dict=self.probability_calculator()
        weight_dict={}
        #contains the number of independent parameters
        

        for tw in range(1,self.tree_width+1):
            weight_dict[tw]={}
            for k,v in stat_dict[tw].iteritems():
                tmp=0
                param_number=0
                for comb in v.keys():
                    if probs_dict[tw][k][comb] >0:
                        
                        if comb[len(comb)-1]=='1' and comb[0:len(comb)-1].count('1') ==len(comb)-1:
                            tmp+=stat_dict[tw][k][comb]*math.log(probs_dict[tw][k][comb])
                            param_number+=1
                            
                        if comb[len(comb)-1]=='0' and  comb[0:len(comb)-1].count('1') ==len(comb)-1:
                            tmp+=stat_dict[tw][k][comb]*math.log(probs_dict[tw][k][comb])
                            
                            
                            
                                                        
                        if comb[len(comb)-1]=='1' and comb[0:len(comb)-1].count('1') !=len(comb)-1 and (probs_dict[tw][k][comb] <= self.epsilon):
                            tmp+=stat_dict[tw][k][comb]*math.log(probs_dict[tw][k][comb])
                            param_number+=1
                            
                        if comb[len(comb)-1]=='0' and  comb[0:len(comb)-1].count('1') !=len(comb)-1 and probs_dict[tw][k][comb[0:len(comb)-1]+'1'] <= self.epsilon:   
                            tmp+=stat_dict[tw][k][comb]*math.log(probs_dict[tw][k][comb])  
                            
                            
                            
                            
                        if comb[len(comb)-1]=='1' and comb[0:len(comb)-1].count('1') !=len(comb)-1 and (probs_dict[tw][k][comb] > self.epsilon):
                            tmp+=stat_dict[tw][k][comb]*math.log(self.epsilon)

                        if comb[len(comb)-1]=='0' and  comb[0:len(comb)-1].count('1') !=len(comb)-1 and probs_dict[tw][k][comb[0:len(comb)-1]+'1'] > self.epsilon:
                            tmp+=stat_dict[tw][k][comb]*math.log(1-self.epsilon)     
                              

                    else:
         
                        tmp+=math.log(self.epsilon*1e-10)
             
            
                        
                weight_dict[tw][k]=tmp-0.5*math.log(len(self.data))*math.pow(2, int(tw))
       
        return weight_dict
    
    


    def semi_monotone_weight_calculator(self):

        stat_dict=self.stat_dict_maker()
        probs_dict=self.probability_calculator()
        weight_dict={}
        ep=0.25

        for tw in range(1,self.tree_width+1):
            weight_dict[tw]={}
            for k,v in stat_dict[tw].iteritems():
                tmp=0

                for comb in v.keys():
                    if probs_dict[tw][k][comb] >0:

                        if comb[len(comb)-1]=='1' and comb[0:len(comb)-1].count('0') ==len(comb)-1 and (probs_dict[tw][k][comb] > self.epsilon):
                            tmp+=stat_dict[tw][k][comb]*math.log(self.epsilon)
                            
                        if comb[len(comb)-1]=='0' and comb[0:len(comb)-1].count('0') ==len(comb)-1 and probs_dict[tw][k][comb[0:len(comb)-1]+'1'] > self.epsilon:
                            tmp+=stat_dict[tw][k][comb]*math.log(1-self.epsilon)
                            
                            
                            
                            
                        if comb[len(comb)-1]=='1' and comb[0:len(comb)-1].count('0') ==len(comb)-1 and (probs_dict[tw][k][comb] <= self.epsilon):
                            tmp+=stat_dict[tw][k][comb]*math.log(probs_dict[tw][k][comb])
                            
                            
                        if comb[len(comb)-1]=='0' and comb[0:len(comb)-1].count('0') ==len(comb)-1 and probs_dict[tw][k][comb[0:len(comb)-1]+'1'] <= self.epsilon:
                            tmp+=stat_dict[tw][k][comb]*math.log(probs_dict[tw][k][comb])
                            
                            
                            
                            
                        if comb[len(comb)-1]=='1' and comb[0:len(comb)-1].count('0') !=len(comb)-1:
                            tmp+=stat_dict[tw][k][comb]*math.log(probs_dict[tw][k][comb])
                        
                
                        if comb[len(comb)-1]=='0' and comb[0:len(comb)-1].count('0') !=len(comb)-1:
                            tmp+=stat_dict[tw][k][comb]*math.log(probs_dict[tw][k][comb])
                            
                    else:

                        tmp+=math.log(self.epsilon*1e-10)
                        

                weight_dict[tw][k]=tmp-0.5*math.log(len(self.data))*(math.pow(2, int(tw)))
                #weight_dict[tw][k]=tmp-0.5*math.log(len(self.data))*param_number
        return weight_dict
    
    
    
    
    
    def root_less_hedge_finder(self,parent_set,child_vertex,weight_dict):
        # this function searches for the hyperedge corresponding to the root-less edge
        for edg,weight in weight_dict[len(parent_set)].iteritems():
            edg_vertex_list=edg.split(',')
            edg_parent_set=set(edg_vertex_list[0:len(edg_vertex_list)-1])
            edg_child=edg_vertex_list[len(edg_vertex_list)-1]
            if (edg_parent_set==parent_set) and (edg_child==child_vertex):
                #ed=edg
                #print edg,weight
                we=weight
                break
        return we
    
    def root_effect_remover(self,weight_dict):

        used_hyperedges_set=set([])
        
        
        vertex_stat_dict=self.vertex_stat_dict_maker()        
        roots_set=set([])
        
        for r in range(self.tree_width):
            roots_set.add(str(r))
            
        filtered_weight_dict={}
        for tw in range(1,self.tree_width+1):
            filtered_weight_dict[tw]={}
            
            for k,v in weight_dict[tw].iteritems():
                vertex_list=k.split(',')
                parents_list=vertex_list[0:len(vertex_list)-1]
                parents_set=set(parents_list)
                child_vertex=vertex_list[len(vertex_list)-1]

                #r_in_edge is the set of all root parents that are in the hyperedge 
                r_in_edge=roots_set.intersection(parents_set)
                if len(r_in_edge)==0:
                    # this is the normal case that no root parents are in the parents
                    filtered_weight_dict[tw][k]=weight_dict[tw][k]
                #in this case we can simply remove the roots
                elif (len(r_in_edge)!=0) and (len(r_in_edge) < len(parents_set)):

                    parents_without_r=parents_set.difference(roots_set)

                    

                
                    tmp_list=[]
                    tmp_str=''
                    for p in parents_without_r:
                        tmp_list.append(int(p))
                        tmp_list.sort()
                    for l in tmp_list:
                        tmp_str+=str(l)+','
                    tmp_str+=child_vertex
                  
                    if not (tmp_str in used_hyperedges_set):
                        
                        we=self.root_less_hedge_finder(parents_without_r, child_vertex, weight_dict)
                        filtered_weight_dict[tw][k]=we
                        used_hyperedges_set.add(tmp_str)
                #this is the case in which all the parents are roots
                elif len(r_in_edge)==len(parents_set) and (not(child_vertex in used_hyperedges_set)):
                    #print 'len(r_in_edge)==len(parents_set):'
                    v_0=vertex_stat_dict[child_vertex]['0']
                    v_1=vertex_stat_dict[child_vertex]['1']  
                    n=len(self.data)  # the number of data points    

                    if v_0 ==0:
                        v_0=1
                    if v_1==0:
                        v_1=1
                               
                 
                    new_weight=v_0*math.log(float(v_0)/n) + v_1*math.log(float(v_1)/n)-0.5*math.log(len(self.data))
                    filtered_weight_dict[tw][k]=new_weight
                    used_hyperedges_set.add(child_vertex)
        return filtered_weight_dict
    

def synthetic_data_file_reader(file_path):
    data=[]
    infile=open(file_path)
    for line in infile:
        tmp=line.strip().split(',')
        data.append(tmp)
    return data              
    


    
    
    
    
    
    
    
    
    
    
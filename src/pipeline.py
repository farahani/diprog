import sys
#import check_Tumor_generator
import dfs
import C_var_length_MILP_writer
import C_var_len_weight_calculator
import networkx as nx
import cplex
from cplex.exceptions import CplexSolverError
import time

from threading import Thread
from cplex.callbacks import MIPInfoCallback
import networkx as nx


class WatcherThread(Thread):
    def set_time(self,sleep_time):
        self.sleep_time=sleep_time

    def run(self):

        time.sleep(self.sleep_time)

        # ... and abort the CPLEX optimization.
        print "Aborting CPLEX via a cplex.terminate() call."
        cplex.terminate()
        


class C_MILP_solver:

    def __init__(self,lp_file_path):
        self.lp_file_path=lp_file_path

        
        
    def MILP_solver_time_limit(self,time_lim,memory_limit):
        lp_file_path=self.lp_file_path
        directed_simple_edg_set=set([])
        simple_edges_set=set([])
        hyper_edges_set=set([])

        
        c = cplex.Cplex(lp_file_path)
        ###
        c.parameters.mip.limits.treememory.set(memory_limit)
        
        
        watcherthread = WatcherThread()
        watcherthread.set_time(time_lim)
        watcherthread.start()
        c.solve()
        sol = c.solution
        if sol.is_primal_feasible():
            x = c.solution.get_values(0, c.variables.get_num()-1)
            for j in range(c.variables.get_num()):
                if x[j]!=0 and c.variables.get_names(j).find('s')!=-1:
                    simple_edges_set.add(c.variables.get_names(j))
                if x[j] > 0.9 and c.variables.get_names(j).find('h')!=-1:
                    #print c.variables.get_names(j)+'  '+str(x[j])
                    hyper_edges_set.add(c.variables.get_names(j))
     
        for hyperedge in hyper_edges_set:
            tmp=hyperedge.split('_')[1:]
            parent_vertices=tmp[0:len(tmp)-1]
            child_vertex=tmp[len(tmp)-1]
            for p in parent_vertices:
                directed_simple_edg_set.add(str(p)+'_'+str(child_vertex))
            
        
        return directed_simple_edg_set,hyper_edges_set
    
    
class cancer_data_reader:
    def __init__(self,data_file,tree_width):
        self.file_path=data_file
        self.tree_width=int(tree_width)

    
    def file_reader(self):

        infile=open(self.file_path)
        abberation_dict={}
        tumores_list=[]
        tumores=infile.readlines()

        abbs_list=tumores[0].strip().split(',')  
        print abbs_list
        
        for q in range(len(abbs_list)):
            abbs_list[q]=abbs_list[q].strip()
        
        # adding the root parents
        for r in range(self.tree_width):
            abbs_list.insert(0, str(r))
            
        # making the dictionary of aberration names
        for i in range(len(abbs_list)):
            abberation_dict[i]=abbs_list[i]
        #print abberation_dict
            
        counter=0
        # traversing the rest of tumors
        for j in range(1,len(tumores)):

            temp=tumores[j].strip().split(',')
        
            for z in range(len(temp)):
                temp[z]=temp[z].strip()
                
            #adding root parents to each tumor
            for i in range(self.tree_width):
                temp.insert(0,'1')
                
            
            final_temp=[]
            for r in range(len(temp)):
                if temp[r]=='1':# or (int(r) in range(self.tree_width)):
                    final_temp.append(str(r))
             
            # rejects the tumors without any aberration        
            #if len(final_temp)>self.tree_width:   
            tumores_list.append(final_temp)
            if len(final_temp)<=self.tree_width:
                counter+=1
                

        return tumores_list,abberation_dict
        

  



def cancer_dag_maker(recovered_edges_set,tree_width,abberation_dict,path,x_tonicity):
    
    with_root_labeled_G=nx.DiGraph()
    without_root_labeled_G=nx.DiGraph()
    # making the roots set
    roots_set=set([])
    for r in range(tree_width):
        roots_set.add(str(r))
        
    for edg in recovered_edges_set:
        tmp=edg.split('_')
        node_label_0=abberation_dict[int(tmp[0])]
        node_label_1=abberation_dict[int(tmp[1])]
        if tmp[0] not in roots_set:
            without_root_labeled_G.add_node(tmp[0],label=node_label_0)       
            without_root_labeled_G.add_node(tmp[1],label=node_label_1)
            without_root_labeled_G.add_edge(tmp[0],tmp[1])
            # the case for without root is the same 
            with_root_labeled_G.add_node(tmp[0],label=node_label_0)       
            with_root_labeled_G.add_node(tmp[1],label=node_label_1)
            with_root_labeled_G.add_edge(tmp[0],tmp[1])
        if tmp[0] in roots_set:
            # the first line is just to add the separated node
            without_root_labeled_G.add_node(tmp[1],label=node_label_1) 
            with_root_labeled_G.add_node(tmp[0],label=node_label_0)       
            with_root_labeled_G.add_node(tmp[1],label=node_label_1)
            with_root_labeled_G.add_edge(tmp[0],tmp[1])
    
    nx.write_dot(without_root_labeled_G,path+x_tonicity+'_'+'without_root.dot')        
    nx.write_dot(with_root_labeled_G,path+x_tonicity+'_'+'with_root.dot')
    #return without_root_labeled_G, with_root_labeled_G
        
def external_dag_maker(recovered_edges_set,abberation_dict,path):
    outfile=open(path,'w')
    for edg in recovered_edges_set:
        tmp=edg.split('_')
        #if tmp[0] not in roots_set:
        node_label_0=abberation_dict[int(tmp[0])]
        node_label_1=abberation_dict[int(tmp[1])]
        
        outfile.write(tmp[0]+','+node_label_0+','+tmp[1]+','+node_label_1+'\n')
    outfile.close()
    
    
    
def dag_writer(graph,path):
    outfile=open(path,'w')
    for edg in graph.edges():
        outfile.write(edg[0]+','+edg[1]+'\n')
    outfile.close()

    
        
def dag_maker(recovered_edges_set,tree_width,abberation_dict,path,x_tonicity,epsilon):
    with_root_labeled_G=nx.DiGraph()
    without_root_labeled_G=nx.DiGraph()
    # making the roots set
    roots_set=set([])
    for r in range(tree_width):
        roots_set.add(str(r))
        
    for edg in recovered_edges_set:
        tmp=edg.split('_')
        node_label_0=abberation_dict[int(tmp[0])]
        node_label_1=abberation_dict[int(tmp[1])]
        if tmp[0] not in roots_set:
            without_root_labeled_G.add_node(node_label_0)       
            without_root_labeled_G.add_node(node_label_1)
            without_root_labeled_G.add_edge(node_label_0,node_label_1)
            # the case for without root is the same 
            with_root_labeled_G.add_node(node_label_0)       
            with_root_labeled_G.add_node(node_label_1)
            with_root_labeled_G.add_edge(node_label_0,node_label_1)
        if tmp[0] in roots_set:
            # the first line is just to add the separated node
            without_root_labeled_G.add_node(node_label_1) 
            with_root_labeled_G.add_node(node_label_0)       
            with_root_labeled_G.add_node(node_label_1)
            with_root_labeled_G.add_edge(node_label_0,node_label_1)
    
    
    dag_writer(without_root_labeled_G,path+'/'+x_tonicity+'_'+str(tree_width)+'_'+str(epsilon)+'_'+'edges.csv')
    #dag_writer(with_root_labeled_G,path+'/'+x_tonicity+'_'+str(tree_width)+'_'+str(epsilon)+'_'+'with_root.csv')
    nx.write_gml(without_root_labeled_G,path+'/'+x_tonicity+'_'+str(tree_width)+'_'+str(epsilon)+'_'+'DAG.gml')
    return without_root_labeled_G
 


                
def statistics_writer(x_tonicity,tw,epsilon,outfile_path,BIC):
    
    outfile=open(outfile_path+'/'+str(tw)+'_'+str(epsilon)+'_'+x_tonicity+'_summary.csv','w')
    outfile.write('x_tonicity,max parent no, epsilon, BIC score \n')
    outfile.write(x_tonicity+','+str(tw)+','+str(epsilon)+','+str(BIC)+'\n')
    outfile.close()
    

    
    
    
    
def log_likelihood_calculator(weight_dict,hyper_edge_set):
    lg=0
    parents_set=set([])
    for edg in hyper_edge_set:
        tmp=edg.split('_')[1:]
        child=tmp[len(tmp)-1]
        parents_set=set(tmp[0:len(tmp)-1])
        parents_number=len(parents_set)
        for k,v in weight_dict[parents_number].iteritems():
            w_parents=set(k.split(',')[0:parents_number])
            w_child=k.split(',')[parents_number]
            if child==w_child and w_parents==parents_set:
                lg+=v
                break
    return lg

    
    
    
def pipe(dataset_path,output_path,tree_width,epsilon,waiting_time,x_tonicity,memory_limit):
    
    cdr=cancer_data_reader(dataset_path,tree_width)
    data,abberation_dict=cdr.file_reader()
    vertex_number=len(abberation_dict.keys())-int(tree_width)
    t=C_var_len_weight_calculator.C_weight_calculator(tree_width,vertex_number,data,epsilon) 
    
    if x_tonicity=='MPN':
        mono_weight=t.monotone_weight_calculator()
        mono_filtered=t.root_effect_remover(mono_weight)
        MPN_w=C_var_length_MILP_writer.C_MILP_writer(tree_width,vertex_number,mono_filtered,output_path+'/'+str(tree_width)+'_'+str(epsilon)+'_MPN.lp')
        MPN_w.problem_writer()
        MPN_solver=C_MILP_solver(output_path+'/'+str(tree_width)+'_'+str(epsilon)+'_MPN.lp')
        MPN_edg_Set,MPN_hyperedge_set=MPN_solver.MILP_solver_time_limit(float(waiting_time),float(memory_limit))
        mono_learnt_graph=dag_maker(MPN_edg_Set, tree_width, abberation_dict, output_path,'MPN', epsilon)

        lg=log_likelihood_calculator(mono_filtered, MPN_hyperedge_set)
        statistics_writer('MPN', tree_width, epsilon, output_path,lg)
        
        
        
    if x_tonicity=='SMPN':
        semi_mono_weight=t.semi_monotone_weight_calculator()
        semi_mono_filtered=t.root_effect_remover(semi_mono_weight)
            #print semi_mono_filtered
        SMPN_w=C_var_length_MILP_writer.C_MILP_writer(tree_width,vertex_number,semi_mono_filtered,output_path+'/'+str(tree_width)+'_'+str(epsilon)+'_SMPN.lp')
        SMPN_w.problem_writer()
        SMPN_solver=C_MILP_solver(output_path+'/'+str(tree_width)+'_'+str(epsilon)+'_SMPN.lp')
        SMPN_edg_Set,SMPN_hyperedge_set=SMPN_solver.MILP_solver_time_limit(float(waiting_time),float(memory_limit))
            
    
        semi_learnt_graph=dag_maker(SMPN_edg_Set, tree_width, abberation_dict, output_path,'SMPN', epsilon)

        lg=log_likelihood_calculator(semi_mono_filtered, SMPN_hyperedge_set)
        statistics_writer('SMPN', tree_width, epsilon, output_path,lg)
        
    if x_tonicity=='General':
        general_weight=t.weight_calculator()
        general_filtered=t.root_effect_remover(general_weight)
        General_w=C_var_length_MILP_writer.C_MILP_writer(tree_width,vertex_number,general_filtered,output_path+'/'+'_'+str(tree_width)+'_'+str(epsilon)+'_General.lp')
        General_w.problem_writer()
        General_solver=C_MILP_solver(output_path+'/'+'_'+str(tree_width)+'_'+str(epsilon)+'_General.lp')
        General_edg_Set,General_hyperedge_set=General_solver.MILP_solver_time_limit(float(waiting_time),float(memory_limit))
        g_learnt_graph=dag_maker(General_edg_Set, tree_width, abberation_dict, output_path,'General', epsilon)
        lg=log_likelihood_calculator(general_filtered, General_hyperedge_set)
        statistics_writer('General', tree_width, epsilon, output_path,lg)
    
    
    
    
    
    
    
    
    
    
    

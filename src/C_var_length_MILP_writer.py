import random
import math


class C_MILP_writer:
    def __init__(self,tree_width,vertex_number,bags_weight_dict,path_to_output):
        self.output_path=path_to_output
        #self.data_path=path_to_data
        self.bags_weight_dict=bags_weight_dict
        self.vertex_number=vertex_number
        self.tree_width=tree_width

        

    def hyper_edge_from_weight_dict(self,bags_weight_dict,tree_width):
        # returns the set of list of h_edge vertices hedge_set=set(['1','3','4']) etc. 
        hedge_set=set([])
        for tw in range(1,tree_width+1):
            for k in bags_weight_dict[tw].keys():
                tmp=k.split(',')
                hedge_set.add(tmp)
        return hedge_set
        
        
        
    def comb_list(self,seq, k):
        "returns a list of all k-combinations of the elements of sequence seq"
        n=len(seq)
        if not 0<=k<=n:
            raise Exception,"0<=k<=len(seq) is not true"
        v=[]   #list of combinations

        def f(x,y,a):
            if x==k:
                #we have taken enough elements, reject all remaining elements
                v.append(a)
                return
            if y==n-k:
                #we have rejected enough elements, take all remaining elements
                a.extend(seq[x+y:])
                v.append(a)
                return
            if (x<k):
                #take element seq[x+y]
                h=a+[seq[x+y]]
                f(x+1,y,h)
            if (y<n-k):
                #don't take element seq[x+y]
                f(x,y+1,a)            
        f(0,0,[])
        #print len(v)
        return v
    
      
    def h_maker(self,vertices):
        #gets the vertex list of a hyper edge in the form of a list and returns a string j_i_j_k
        vertex_list=vertices.split(',')
        h='h'
        for v in vertex_list:
            h+='_'+v
        return h
    
    
    
    def s_maker(self,comb_list):
        #takes a list of [[i,j],[z,k]] combinations and returns a list of s_i_j that always i < j
        res_list=[]
        for comb in comb_list:
            l=[int(comb[0]),int(comb[1])]
            l.sort()
            s='s_'+str(l[0])+'_'+str(l[1])
            res_list.append(s)
        return res_list
    

        
        
            
    def cond_1(self,weight_dict):
        # for each non-root node the cond1_dict stores the set of equations for that node
        cond_1_dict={}
        
        # initializing the cond_dict for each non-root vertex
        # root parents are not counted in the vertex number
        for v in range(self.tree_width,self.vertex_number+self.tree_width):
            cond_1_dict[str(v)]=''
            
        root_vertices=range(self.tree_width)
        for tw in weight_dict.keys():
            for hyper_edge,weight in weight_dict[tw].iteritems():
                child_vertex=hyper_edge.split(',')[len(hyper_edge.split(','))-1]
                # checks for non-root vertices
                if int(child_vertex) >= self.tree_width:

                    #building the string for h
                    h_tmp='h'
                    for vertex in hyper_edge.split(','):
                        h_tmp+='_'+vertex
                    #adding the new h_i_j_k to the equation set
                    cond_1_dict[child_vertex]+='+ '+h_tmp
        # finalizing the equation set for cond_1
        for k,v in cond_1_dict.iteritems():
            cond_1_dict[k]=cond_1_dict[k][2:]+' = 1'
            
        return cond_1_dict
                      
                    
    def cond_2(self,weight_dict):

        cond_2_dict={}
        for tw in weight_dict.keys():
            for hyper_edge in weight_dict[tw].keys():
                #initializes the equation set for each hyperedge
                cond_2_dict[hyper_edge]=[]
                
                vertex_list=hyper_edge.split(',')
                parents_list=vertex_list[0:len(vertex_list)-1]
                child_vertex=vertex_list[len(vertex_list)-1]
                # equation regarding special parent vertex
                eq=''
                for p in parents_list:
                    eq= 'O_'+p+' - O_'+child_vertex+' + '+self.h_maker(hyper_edge)+ '< 0.999'
                    cond_2_dict[hyper_edge].append(eq)
                    # mid # of sd[1,4,7]=='1_2'
        return cond_2_dict
    

        
    def cond_3(self,weight_dict):
        # h_e - s_i_j <= 0
        # for practical issues we consider i < j although s_ij = s_ji
        cond_3_dict={}
        for tw in weight_dict.keys():
            for hyper_edge in weight_dict[tw].keys():
                cond_3_dict[hyper_edge]=[]
                
                vertex_list=hyper_edge.split(',')
                vertex_2_combs=self.comb_list(vertex_list, 2)
                s_list=self.s_maker(vertex_2_combs)
                h_e=self.h_maker(hyper_edge)
                for s_i_j in s_list:
                    eq=''
                    eq+=h_e+' - '+s_i_j
                    eq+= ' <= 0'
                    cond_3_dict[hyper_edge].append(eq)
        
        return cond_3_dict
    

    
    def cond_4_BACK(self,weight_dict):
        cond_4_dict={}

        all_vertices_list=range(self.vertex_number+self.tree_width)
        total_edge_list=self.comb_list(all_vertices_list, 2)
        

        nonroot_edge_list=[]
        # removing the s_i_j that both i and j are in the root parents
        for edg in total_edge_list:
            if not(edg[0] < self.tree_width and edg[1] < self.tree_width):
                nonroot_edge_list.append(edg)
        
        # contains the list of all possible s_i_j s
        total_s_i_j_list=self.s_maker(nonroot_edge_list)

        for s_i_j in total_s_i_j_list:
            simpledge_vertex_set=set(s_i_j.split('_')[1:])            
            eq=s_i_j
            
            # traversing all hyperedges 
            for tw in weight_dict.keys():
                for hyperedge in weight_dict[tw].keys():
                    hyperedge_vertex_list=hyperedge.split(',')
                    h_ijk=self.h_maker(hyperedge)

                    child_vertex=hyperedge_vertex_list[len(hyperedge_vertex_list)-1]
                    parent_vertex_list=hyperedge_vertex_list[0:len(hyperedge_vertex_list)-1]

                    second_vertex=simpledge_vertex_set.difference(child_vertex)

                    if (child_vertex in simpledge_vertex_set) and (second_vertex.issubset(set(parent_vertex_list))):
                        eq+= '- '+h_ijk
                        
            cond_4_dict[s_i_j]=eq + ' <= 0'
            
        return cond_4_dict
    
    
    
    def cond_4(self,weight_dict):
        cond_4_dict={}

        all_vertices_list=range(self.vertex_number+self.tree_width)
        total_edge_list=self.comb_list(all_vertices_list, 2)
        

        nonroot_edge_list=[]
        # removing the s_i_j that both i and j are in the root parents
        for edg in total_edge_list:
            if not(edg[0] < self.tree_width and edg[1] < self.tree_width):
                nonroot_edge_list.append(edg)
        
        # contains the list of all possible s_i_j s
        total_s_i_j_list=self.s_maker(nonroot_edge_list)

        for s_i_j in total_s_i_j_list:
            simpledge_vertex_set=set(s_i_j.split('_')[1:])            
            eq=s_i_j
            
            # traversing all hyperedges 
            for tw in weight_dict.keys():
                for hyperedge in weight_dict[tw].keys():
                    hyperedge_vertex_list=hyperedge.split(',')
                    h_ijk=self.h_maker(hyperedge)

                    child_vertex=hyperedge_vertex_list[len(hyperedge_vertex_list)-1]
                    parent_vertex_list=hyperedge_vertex_list[0:len(hyperedge_vertex_list)-1]

                    second_vertex=simpledge_vertex_set.difference(set([child_vertex]))
                    #print second_vertex
                    #print simpledge_vertex_set,child_vertex,second_vertex
                    #if h_ijk=='h_2_10':
                     #   print h_ijk
                    if (child_vertex in simpledge_vertex_set) and (second_vertex.issubset(set(parent_vertex_list))) and len(second_vertex)==1:
                        eq+= '- '+h_ijk
                        #if h_ijk=='h_10_2':
                         #   print h_ijk
                        
            cond_4_dict[s_i_j]=eq + ' <= 0'
            
        return cond_4_dict
    
    

    
    def cond_5(self):
        #simple edges between the root parents are always positive
        cond_5_list=[]
        root_list=range(self.tree_width)
        if self.tree_width >=2:
            root_edge_list=self.comb_list(root_list, 2)
            
        root_s_i_j_list=self.s_maker(root_edge_list)
        for s_ij in root_s_i_j_list:
            cond_5_list.append(s_ij+' = 1')
        return cond_5_list
    
    def cond_6(self):
        cond_6_list=[]
        root_list=range(self.tree_width)
        for r in root_list:
            cond_6_list.append('O_'+str(r)+' = 0')
        return cond_6_list
    
    
    
    

        
    
    def line_spliter(self,line,cnd_no):
        # this function splits the long line to lines with max 500 length to make it compliance with 
        # the lp standard
        
        res=[]
        if cnd_no=='objective':
            tmp=line[1:].strip().split('-')
            buff=''
            for v in tmp:
                if len(buff) < 400:
                    buff+='-'+v
                else:
                    res.append(buff+'-'+v)
                    buff=''
            res.append(buff)


        if cnd_no=='cnd_1':

            tmp=line.strip().split('+')
            buff=tmp[0]
            for v in tmp[1:]:
                if len(buff)<400 and v.find('=')==-1:
                    buff+='+'+v
                else:
                    res.append(buff+'+'+v)
                    buff=''        

    
        if cnd_no=='cnd_4':
            #print 'line',line
            tmp=line.strip().split('-')
            buff=tmp[0]
            for v in tmp[1:]:
                if len(buff)<400 and v.find('=')==-1:
                    buff+='-'+v
                else:
                    res.append(buff+'-'+v)
                    buff=''
                    

  
        return res    
    
    
        
    
    def problem_writer(self):
        path=self.output_path
        weight_dict=self.bags_weight_dict
        node_number=self.vertex_number
        
        outfile=open(path,'w')
        outfile.write('maximize'+'\n')
        temp=''
        
        #writing the objective function       
        for tw in self.bags_weight_dict.keys():
            for hyper_edge,weight in self.bags_weight_dict[tw].iteritems():
                h_ijk=self.h_maker(hyper_edge)

                temp +=' '+str(weight)+h_ijk
             
        if len(temp[1:]) <400:
            outfile.write(temp[1:]+'\n')
        else:
            lines_list=self.line_spliter(temp[1:], 'objective')
            for line in lines_list:
                outfile.write(line+'\n')


        # writing the conditions        
        outfile.write('subject to \n')
        # condition 1
        cnd_1=self.cond_1(self.bags_weight_dict)
        for k,v in cnd_1.iteritems():
            if len(v) < 400:
                outfile.write(v+'\n')
            else:
                lines_list=self.line_spliter(v, 'cnd_1')
                for line in lines_list:
                    outfile.write(line+'\n')           
            
        
        #condition 2
        cnd_2=self.cond_2(self.bags_weight_dict)
        for k,v in cnd_2.iteritems():
            for i in range(len(v)):
                outfile.write(v[i]+'\n')
                if len(v) > 650:
                    print 'warning 2!'
                

            
        #condition 6
        cnd_6=self.cond_6()
        for eq in cnd_6:
            outfile.write(eq+'\n')
            
            
        # writing the bounds
        outfile.write('Bounds' +'\n')
        for i in range(self.tree_width,self.tree_width+self.vertex_number):
            outfile.write('0<= O_'+str(i)+' <= 1'+'\n')
            
        #writing binary conditions    
        outfile.write('Binary'+'\n')
        # writing h_ijk
        j=0
        for tw in self.bags_weight_dict.keys():
            for hyper_edge in self.bags_weight_dict[tw].keys():
                j+=1
                outfile.write(self.h_maker(hyper_edge)+'\t')
                if j==5:
                    j=0
                    outfile.write('\n')
                    

        outfile.write('\n')
        outfile.write('end')
        outfile.close()
        
            
                    
    
        
def synthetic_data_file_reader(file_path):
    data=[]
    infile=open(file_path)
    for line in infile:
        tmp=line.strip().split(',')
        data.append(tmp)
    return data   
  


       
        
        
        
        
        
        
        
        
        
        
    
    
    
#!/usr/bin/env python
import argparse
import pipeline


        
if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-n","--networkType" ,type=str, choices=['MPN','SMPN','General'], help=' this is network type')
    parser.add_argument('-p','--maxParents',type=int, help='Maximum number of parents.')
    parser.add_argument('-d','--dataset_path',help='Path to the dataset')
    parser.add_argument('-e','--epsilon',type=float, help='Value of epsilon for MPN and SMPNs')
    parser.add_argument('-t','--milp_time',help='Maximum time allowed to CPLEX')
    parser.add_argument('-m','--memory_limit',help='Maximum memory allowed for CPLEX in Mega-bytes')
    parser.add_argument('-o','--output_path',help='Path to a folder for writing the results')
    args = parser.parse_args()
    pipeline.pipe(args.dataset_path, args.output_path, args.maxParents, args.epsilon, float(args.milp_time), args.networkType,float(args.memory_limit))
    
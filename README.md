DiProg : Software for learning Disease Progression networks by reducing to MILP
-----------------------------------------------------------------------------------------------------

-Version 1.0, June 2012

Hossein Farahani

farahani@kth.se
-----------------------------------------------------------------------------------------------------

External dependencies:

1. CPLEX and CPLEX python API: 
IBM ILOG CPLEX optimization library can be downloaded from IBM. This software is free for academic use. 
It can be download from IBM Academic Initiative from:
https://www.ibm.com/developerworks/university/software/get_software.html

2. NetworkX:
Networkx is a Python package for studying graphs. It can be downloaded for free from:
http://networkx.lanl.gov/download.html

3. Python 2.7 or above
-----------------------------------------------------------------------------------------------------

Installation:
------------------

Just decompress and unpack the files in your local file system. DiProg.py is located in the src folder. 


Input file format and sample datasets:
------------------------
Input file should be csv file, with the name of aberrations/variables in the first line. Each subsequent line is a tumor. 1 represents existence of an aberration in a tumor and 0 its absence. 
In the directory datasets. There are 3 datasets as the following:

BC.csv: This is a breast cancer datasets from 
Hoglund, M., Gisselsson, D., Hansen, G. & Soll, T. Multivariate analysis of chromosomal imbalances in breast cancer delineates cytogenetic pathways and reveals complex relationships among imbalances. Cancer research (2002).

RCC.csv: This is a renal cell carcinoma (RCC) dataset from 
Hoglund, M., Gisselsson, D. & Soller, M. Dissecting karyotypic patterns in renal cell carcinoma: an analysis of the accumulated cytogenetic data. Cancer genetics and � (2004).

Small_RCC.csv:
This a another dataset from renal cell carcinoma with less variables comparing to RCC.csv. It is used int he following paper. 
Gerstung, M., Baudis, M., Moch, H. & Beerenwinkel, N. Quantifying cancer progression with conjunctive Bayesian networks. Bioinformatics 25, 2809�2815 (2009).

Usage:
------------------
python DiProg.py

-h help

-n The type of the learned network {MPN,SMPN,General}
-p maximum number of parents. Note that in a k-bounded hyper-graph the maximum number of parents is k-1
-d path to the dataset
-e value of epsilon for learning MPN and SMPNs. Note that if you want to learn a General network choose an arbitrary value for epsilon. DiProg will ignore it. 
-t The maximum time allowed for DiProg to solve the problem in seconds. Upon reaching time DiProg interrupts optimization in CPLEX and picks the best solution in the CPLEX solution pool. 
-o output path
This is path to the folder for writing the output.


Output files:
-------------------

*.gml:
 It is the progression network that is learned from the data in gml format. gml format is a popular format for graphs. There are many free softwares available for reading this format such as yEd.
 yEd can be obtained from: 
 http://www.yworks.com/en/products_yed_download.html 

*.lp:
This file contains the MILP problem and is readable by CPLEX. It is used internally. Advanced users can use this file to solve the problem with their customized CPLEX configuration. 

*.summary.csv
Contains the BIC score of the learned network.

*edges.csv
A comma separated list of the learned edges

